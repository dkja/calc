/**
 *	Exceptions used by parser
 */
#ifndef _EXCEPTIONS_H_
#define _EXCEPTIONS_H_

#include <stdexcept>
#include <string>
#include "operations.h"

/**
 *  \struct parser_error
 *  General parser exception. Interface for specific expection.
 */
struct parser_error : std::runtime_error
{
    parser_error(const char* text) 
        : std::runtime_error( text )
    {}
    parser_error(std::string text) 
        : std::runtime_error( text )
    {}
};

/**
 *	@struct invalid_operator
 *  Throw if detect unsupported operator
 */
 struct invalid_operator : parser_error
 {
    invalid_operator(char op)
        : parser_error(std::string{"Invalid operator: "}+=op)
    {}
 };

 /**
 *	@struct divide_by_zero
 *  Throw if detect unsupported operator
 */
 struct divide_by_zero : parser_error
 {
     divide_by_zero()
        :parser_error("Cannot divide by zero")
     {}
    divide_by_zero(const char* text) 
        : parser_error( text )
    {}
    divide_by_zero(std::string text) 
        : parser_error( text )
    {}
 };


 struct var_not_found : parser_error
 {
     var_not_found()
        :parser_error("Variable not found")
     {}
    var_not_found(const char* varname) 
        : parser_error( std::string{"Variable not found: "}+=varname )
    {}
    var_not_found(std::string varname) 
        : parser_error(std::string{"Variable not found: "}+=varname )
    {}
 };
#endif // !_EXCEPTIONS_H_
