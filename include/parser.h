/**
 *	Main parser module.
 */
#ifndef _PARSER_H_
#define _PARSER_H_

#include <vector>
#include <map>
#include "token_stream.h"
#include "config.h"
#include "exceptions.h"
#include "IGenerator.h"

/** @class Parser
 *	Main parser module. 
 */
class Parser
{
public:
    
    Parser(Token_stream* ts, IGenerator<>& gen);
    ~Parser() {};
    virtual void exec();
    virtual void exec(unsigned count);
    virtual void exec_all();

    virtual Token_stream* reload_ts(Token_stream* ts);
    

    virtual void declaration();
    virtual void primary();
    virtual void monomial();
    virtual void polynomial();
    virtual void expression();

protected:

    virtual void clear_last_expression();

    Token_stream*   _ts;
    IGenerator<>&     _generator;
};

#endif // !_PARSER_H_