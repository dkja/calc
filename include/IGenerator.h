#ifndef _IGENERATOR_H_
#define _IGENERATOR_H_

#include <iostream>

template<typename Num=double>
class IGenerator
{
public:
  IGenerator(std::ostream& o)
    :_output(o)
    {}

  virtual void add() = 0;
  virtual void sub() = 0;
  virtual void mul() = 0;
  virtual void div() = 0;
  virtual void decl(const std::string& name) = 0;
  virtual void var(const std::string& name) = 0;
  virtual void number(Num num) = 0;

protected:
  std::ostream& _output;
};

#endif // !_IGENERATOR_H_