#ifndef _TOKEN_H_
#define _TOKEN_H_

#include <string>
#include "operations.h"

/**
 *	Store token
 *  \note Can stone only one token (operation, value or literal)
 */
class Token
{
public:
    enum class Kind
    {
        number,
        invalid,
        operation,
        literal
    };

    explicit Token()                :kind(Kind::invalid) {}
    explicit Token(double val)      :kind(Kind::number), value(val) {}
    explicit Token(Operator op)     :kind(Kind::operation), operation(op) {}
    explicit Token(std::string l)   :kind(Kind::literal), literal(l) {}

    Token(const Token& t);
    Token& operator=(const Token& t);


    ~Token() {}

    Kind kind;
    union {
        Operator    operation;
        double      value;
    };
        std::string literal;
};

#endif // !_TOKEN_H_