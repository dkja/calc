/**
 *	Tmp files stream
 */

#ifndef _TMPFSTREAM_H_
#define _TMPFSTREAM_H_

#include <fstream>


/** @class tmpfstream
 *  Temporary file stream. Create a uniuqe temporary file.
 *      Based on std::fstream. The file will be
 *      automatically deleted when it is closed or the program terminates.
 *
 *  @note   Filename will be template with added six characters.
 */
class tmpfstream : public std::fstream
{
  public:
    explicit tmpfstream(const char *templ);
    virtual ~tmpfstream();

    virtual void close();

    virtual std::string filename() const;

  protected:
    std::string _filename;
};

#endif // !_TMPFSTREAM_H_