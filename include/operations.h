#ifndef _OPERATIONS_H_
#define _OPERATIONS_H_

#include <string>

/**
 *	Operator lists
 */
enum Operator : char
{
    quit = 'q',
    print = ';',
    assign = '='
// Other operators is:
//  *, /, +, -, (, )
};

const std::string DECL_OPERATOR = "let";


#endif // !_OPERATIONS_H_