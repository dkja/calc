#ifndef _TOKEN_STREAM_H_
#define _TOKEN_STREAM_H_

#include "token.h"
#include <iostream>

class Token_stream
{
public:
    Token_stream(std::istream& in = std::cin);

    Token get();
    Token next();
    std::istream& ignore(char c);
    bool eof();
protected:
    std::istream&   _input;
    Token           _current;
};

#endif // !_TOKEN_STREAM_H_