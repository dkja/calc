#include "token_stream.h"
#include "exceptions.h"
#include <string>

using namespace std;

/**
 *	Make Token stream based on istream
 *  
 *  @param[in,out] in - Data stream for Token_stream
 */
Token_stream::Token_stream(std::istream& in)
    :_input(in)
    ,_current()  
{

}

/**
 *	Get current token.
 */
Token Token_stream::get()
{
    return _current;
}


/**
 *	Read variable name 
 *  @note Variable names may contain only '_' or alpha-numeric chars.
 */
void read_varname(istream* i, string* s)
{
    char c {};

    while(i->get(c), c=='_' || isalnum(c))
        s->push_back(c);

    i->unget();
}

/**
 *	Get next token
 *  \warning Can throw exceptions
 */
Token Token_stream::next()
{
    char tmp{};
    _input >> tmp;
    switch(tmp)
    {
        case '0': case '1': case '2': case '3': case '4': 
        case '5': case '6': case '7': case '8': case '9':
        {
            _input.putback(tmp);
            double tmp2{};
            _input >> tmp2;
            _current = Token{tmp2};
            break;
        }
        case '+':
        case '-':
        case '*':
        case '/':
        case '(':
        case ')':
        case Operator::assign:
        case Operator::quit:
        case Operator::print:
            _current = Token{ static_cast<Operator>(tmp) };
            break;
        case '_':
        default:
            _input.putback(tmp);
            // literal must start alpha or '_' char.
            if( !isalpha(tmp)  && tmp != '_')
            {
                _current = Token{};
                throw invalid_operator{tmp};
            }
            std::string literal;
            read_varname(&_input, &literal);
            _current = Token{literal};
    }
    return _current;
}

istream& Token_stream::ignore(char c)
{
    return _input.ignore(2 << 16, c);
}

bool Token_stream::eof()
{
    char c{};
    if(_input >> c)
        _input.putback(c);

    return _input.eof();
}