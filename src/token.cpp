#include "token.h"

Token::Token(const Token& t)
    :kind(t.kind)
{
    *this = t;
}

Token& Token::operator=(const Token& t)
{
    this->kind = t.kind;
    switch(kind)
    {
        case Kind::invalid: break;
        case Kind::number:
            value = t.value;
            break;
        case Kind::operation:
            operation = t.operation;
            break;
        case Kind::literal:
            literal.assign(t.literal);
            break;
    }
    return *this;
}