#include "tmpfstream.h"


// use os dependent code
#ifdef __linux__
    #include "../ports/linux/tmpfstream.cpp"

#elif defined(_WIN32) || defined(_WIN64)
    #include "../ports/windows/tmpfstream.cpp"
    
#endif 