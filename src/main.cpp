#include <iostream>
#include <fstream>
#include "parser.h"
#include "JITGenerator.h"
#include "MultiGenerator.h"
#include "RevPolNotation.h"

using namespace std;


void command_prompt()
{
    cout << "\n\n>";    
}

int main(int argc, char* argv[])
{
    if(argc > 1)
    {
        cout << "calculation v" << VERSION_MAJOR << '.'
            << VERSION_MINOR << '.' << VERSION_BUILD << "\n\n"
            << "\tUsage: STDIO ==> calculation ==> STDOUT\n"
            << "\tExample: (5 - 2) * 3 =\n";
        return 0;
    }

    JITGenerator    jit{ cout };
    ofstream        ofile{"revPolNot.txt"};
    RevPolNotation  rpn {ofile};
    MultiGenerator  gens{};
        gens.add_generator(&jit);
        gens.add_generator(&rpn);

    Token_stream ts{ std::cin };
    Parser parser{ &ts, gens };



    while( command_prompt(), !ts.eof())
    {
        try
        {
        parser.exec();

        auto& stack = jit.get_stack();
        while(stack.size() > 0)
            cout << "\t[" << stack.size() << "]: " << stack.pop() << '\n';

        } catch (parser_error e)
        {
            cout << "ERROR: " << e.what();
            jit.get_stack().clear();
        }
    }

    return 0;
}