#include "parser.h"


using namespace std;

Parser::Parser(Token_stream *ts, IGenerator<>& gen)
    : _ts(ts),
    _generator(gen)
{
}



/**
 *	Execute one expression.
 *  @note It handles stream state clearing after parser_error
 */
void Parser::exec()
{
    try{

        this->expression();

    } catch (parser_error &e)
    {
        this->clear_last_expression();
        throw;
    }
}




/**
 *  Execute {count} expressions.
 */
 void Parser::exec(unsigned count)
 {
     for(int x = 0; x < count; ++x)
        this->exec();
 }
 


/**
 *	Execute all expresssion from the stream.
 */
 void Parser::exec_all()
 {
     while(!_ts->eof())
        this->exec();
 }



/**
 *	Change input (Token_stream).
 *  Return old token_stream.
 */
Token_stream *Parser::reload_ts(Token_stream *ts)
{
    Token_stream *old = this->_ts;
    this->_ts = ts;
    return old;
}



void Parser::declaration()
{

    Token t = _ts->get();
    if(t.kind != Token::Kind::literal)
        throw parser_error{"Expected variable name"};
    string name = t.literal;

    t = _ts->next();
    if(t.kind != Token::Kind::operation || t.operation != Operator::assign)
        throw parser_error{"Expected assign operator"};

    polynomial();
    _generator.decl(name); 
}






void Parser::primary()
{
    Token t = _ts->next();

    switch (t.kind)
    {
    case Token::Kind::number:
        _ts->next();
        _generator.number(t.value);
        return;

    case Token::Kind::literal:
        _ts->next();
        if (t.literal == DECL_OPERATOR)
            declaration();
        else
            _generator.var(t.literal);
        return;

    case Token::Kind::operation:
        switch (t.operation)
        {
        case '-':
            _generator.number(0);
            primary();
            _generator.sub();
            return;
        case '(':
        {
            polynomial();
            t = _ts->get();
            _ts->next();
            if (t.kind != Token::Kind::operation || t.operation != ')')
                throw parser_error{"Expected ')' operator"};
                return;
        }
        default:
            throw parser_error{"Expected primary"};
        }

    case Token::Kind::invalid:
    default:
        throw invalid_operator{' '};
    }
}




void Parser::monomial()
{
    primary();
    while (true)
    {
        Token t = _ts->get();
        if (t.kind != Token::Kind::operation)
            return ;
        else
        {
            switch (t.operation)
            {
            case '*':
                primary();
                _generator.mul();
                break;
            case '/':
            {
                primary();
                _generator.div();
                break;
            }
            default:
                return;
            }
        }
    }
}





void Parser::polynomial()
{
    monomial();
    while (true)
    {
        Token t = _ts->get();
        if (t.kind != Token::Kind::operation)
            return;
        else
        {
            switch (t.operation)
            {
            case '+':
                monomial();
                _generator.add();
                break;
            case '-':
                monomial();
                _generator.sub();
                break;
            default:
                return;
            }
        }
    }
}



void Parser::expression()
{
    polynomial();

    Token t = _ts->get();

    if(t.kind != Token::Kind::operation ||
        t.operation != Operator::print)
            throw parser_error{"Expected end of expression"};
}



void Parser::clear_last_expression()
{
    Token t = _ts->get();
    if(
        t.kind == Token::Kind::operation &&
        t.operation == Operator::print
    )
        return;
    
    _ts->ignore(Operator::print);
}