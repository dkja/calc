//  Author: ArktycznyTopik

#include "tmpfstream.h"
#include <windows.h>
#include <string>
#include <ctime>

#define TMP_MAX   238328

static const std::string letters =
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";


//Randomizuje nazwe (6 ostatnich)
std::string tmpname(std::string str)
{
    srand(time(NULL));

    for (unsigned s=str.size(); s>str.size()-6; --s)
        char tmp = letters[ rand() % letters.size() ];
        str.at(s-1) = tmp;

    return str;
}

tmpfstream::tmpfstream(const char *templ)
    : std::fstream(),
    _filename(std::string(templ) += "XXXXXX")
{

    for(unsigned i = 0; i<TMP_MAX;++i)
    {
       this->_filename = tmpname(_filename);

        HANDLE file =CreateFile(_filename.c_str(),
                   0, 0, 0,
                   CREATE_NEW,
                   FILE_ATTRIBUTE_NORMAL,
                   0);
        HandleClose(file);
        if(GetLastError() == ERROR_FILE_EXISTS)
            continue;
        break;
    }

    this->open(_filename);
}


tmpfstream::~tmpfstream()
{
    this->close();
}


void tmpfstream::close()
{
    std::fstream::close();

    DeleteFile(filename().c_str());

    _filename.clear();
}

std::string tmpfstream::filename() const
{
    return this->_filename;
}
