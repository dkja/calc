#include "tmpfstream.h"

#include <string>
#include <cstring>

#include <unistd.h>

using namespace std;


/**
*  @param  templ[in] - template for new filename
*/
tmpfstream::tmpfstream(const char *templ)
    : std::fstream(),
      _filename(string(templ) += "XXXXXX")
{
    char *var_path = new char[_filename.size()];

    strcpy(var_path, _filename.c_str());

    int tmp_fd = mkstemp(var_path);
    ::close(tmp_fd);

    _filename = string(var_path);
    delete[] var_path;
    this->open(_filename);
}

tmpfstream::~tmpfstream()
{
    this->close();
}

/**
 *	Close file. 
 *  @note Delete file.
 */
void tmpfstream::close()
{
    this->fstream::close();
    
    unlink(_filename.c_str());
    _filename.clear();
}


std::string tmpfstream::filename() const
{
    return this->_filename;
}
