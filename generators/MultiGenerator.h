#ifndef _MULTIGENERATOR_H_
#define _MULTIGENERATOR_H_

#include "IGenerator.h"
#include <vector>

class MultiGenerator : public IGenerator<double>
{
  public:
    using num_type = double;

    MultiGenerator()
      :IGenerator(std::cout) {}

    virtual void add() override;
    virtual void sub() override;
    virtual void mul() override;
    virtual void div() override;
    virtual void decl(const std::string &name) override;
    virtual void var(const std::string &name) override;
    virtual void number(num_type num) override;

    virtual void add_generator(IGenerator* gen);
    auto& get_generators();
  protected:
    std::vector<IGenerator*> _generators;
};

#endif // !_MULTIGENERATOR_H_