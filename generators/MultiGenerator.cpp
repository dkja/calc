#include "MultiGenerator.h"

using namespace std;

void MultiGenerator::add()
{
    for (auto *g : _generators)
        g->add();
}

void MultiGenerator::sub()
{
    for (auto *g : _generators)
        g->sub();
}

void MultiGenerator::mul()
{
    for (auto *g : _generators)
        g->mul();
}

void MultiGenerator::div()
{
    for (auto *g : _generators)
        g->div();
}

void MultiGenerator::decl(const string &name)
{
    for (auto *g : _generators)
        g->decl(name);
}

void MultiGenerator::var(const string &name)
{
    for (auto *g : _generators)
        g->var(name);
}

void MultiGenerator::number(num_type num)
{
    for (auto *g : _generators)
        g->number(num);
}

void MultiGenerator::add_generator(IGenerator<>*gen)
{
    _generators.push_back(gen);
}

auto& MultiGenerator::get_generators()
{
    return _generators;
}


