#include "RevPolNotation.h"

using namespace std;

void RevPolNotation::add()
{
    _output << "+ ";
}

void RevPolNotation::sub()
{
    _output << "- ";
}

void RevPolNotation::mul()
{
    _output << "* ";
}

void RevPolNotation::div()
{
    _output << "/ ";
}

void RevPolNotation::decl(const string &name)
{
    _output << "decl_as(" << name << ") ";
}

void RevPolNotation::var(const string &name)
{
    _output << "var(" << name << ") ";
}

void RevPolNotation::number(num_type num)
{
    _output << num <<" ";
}

void RevPolNotation::add_generator(IGenerator<>* gen)
{
    _generators.push_back(gen);
}

auto& RevPolNotation::get_generators()
{
    return _generators;
}


