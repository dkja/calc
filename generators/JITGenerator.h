#ifndef _JITGENERATOR_H_
#define _JITGENERATOR_H_

#include "IGenerator.h"
#include <stack>
#include <map>

template <typename T>
class Stack : public std::stack<T>
{
  public:
	virtual auto pop() -> typename std::stack<T>::reference&
	{
		auto &res = this->top();
		this->std::stack<T>::pop();
		return res;
	}
	auto clear()
	{
		auto res = this->size();
		while(this->size()> 0)
			this->pop(); 
	}
};

class JITGenerator : public IGenerator<double>
{
  public:
	using num_type = double;
	using var_table_type = std::map<std::string, num_type>;

	JITGenerator(std::ostream& o)
		:IGenerator(o) {}

	virtual void add() override;
	virtual void sub() override;
	virtual void mul() override;
	virtual void div() override;
	virtual void decl(const std::string& name) override;
	virtual void var(const std::string& name) override;
	virtual void number(num_type num) override;

	virtual void add_variable(const char* name, num_type val);
	auto& 			 get_stack() { return _stack; };

  protected:
	Stack<num_type> _stack;
	var_table_type  _var_table;
};

#endif // !_JITGENERATOR_H_