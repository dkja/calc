#include "JITGenerator.h"
#include "exceptions.h"

using namespace std;

void JITGenerator::add() 
{
    auto b = _stack.pop();
    auto& a = _stack.top();
    a+=b;
}



void JITGenerator::sub() 
{
    auto b = _stack.pop();
    auto& a = _stack.top();
    a-=b;
}



void JITGenerator::mul() 
{
    auto b = _stack.pop();
    auto& a = _stack.top();
    a*=b;
}



void JITGenerator::div() 
{
    auto b = _stack.pop();
    if(b == 0)
        throw divide_by_zero();
    auto& a = _stack.top();
    a/=b;
}



void JITGenerator::decl(const string& name) 
{
    auto iter = _var_table.find(name);
    auto value = _stack.pop();
    if(iter != _var_table.end() )
    {
        _var_table.at(name) = value;
        return;
    }

    _var_table.insert( { name, value} );
}



void JITGenerator::var(const string& name) 
{
    auto iter = _var_table.find(name);
    if(iter == _var_table.end())
        throw var_not_found(name);
    
    _stack.push( iter->second );
}



 void JITGenerator::add_variable(const char* name, num_type val)
{
    _var_table[name] = val;
}

void JITGenerator::number(num_type num) 
{
    _stack.push(num);
}