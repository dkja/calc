#ifndef _REVPOLNOTATION_H_
#define _REVPOLNOTATION_H_

#include "IGenerator.h"
#include <vector>

class RevPolNotation : public IGenerator<double>
{
  public:
    using num_type = double;

    RevPolNotation(std::ostream& o)
      :IGenerator(o) {}

    virtual void add() override;
    virtual void sub() override;
    virtual void mul() override;
    virtual void div() override;
    virtual void decl(const std::string &name) override;
    virtual void var(const std::string &name) override;
    virtual void number(num_type num) override;


    virtual void add_generator(IGenerator* gen);
    auto& get_generators();
  protected:
    std::vector<IGenerator<>*> _generators;
};

#endif // !_REVPOLNOTATION_H_