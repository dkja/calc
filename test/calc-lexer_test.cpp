#ifndef _LEXER_TEST_H_
#define _LEXER_TEST_H_

#include <gtest/gtest.h>
#include "tmpfstream.h"
#include "token_stream.h"
#include "exceptions.h"

using namespace std;

namespace Unit_tests
{




/**
 *	Lexer unit test
 */
TEST(lexer, token_stream_test)
{
    tmpfstream test_file("/tmp/utest");
    ASSERT_TRUE(test_file.good()) << "Cannot create tmpfile: " << test_file.filename() << '\n';

    test_file << "5*3/ abudabi (+ 0();k iraq  6";
    test_file.flush();
    test_file.sync();
    test_file.seekg(0);

    Token_stream ts{test_file};
    Token t{};

    // TESTING...
    {
        t = Token{};
        EXPECT_EQ(t.kind, Token::Kind::invalid);

        t = ts.next();
        EXPECT_EQ(t.kind, Token::Kind::number);
        EXPECT_DOUBLE_EQ(t.value, 5.0);

        t = ts.next();
        EXPECT_EQ(t.kind, Token::Kind::operation);
        EXPECT_EQ(t.operation, '*');

        t = ts.next();
        EXPECT_EQ(t.kind, Token::Kind::number);
        EXPECT_DOUBLE_EQ(t.value, 3.0);

        t = ts.next();
        EXPECT_EQ(t.kind, Token::Kind::operation);
        EXPECT_EQ(t.operation, '/');

        {
            t = ts.next();
            EXPECT_EQ(t.kind, Token::Kind::literal);
            auto str = t.literal.c_str();
            EXPECT_STREQ(str, "abudabi");
        }

        t = ts.next();
        EXPECT_EQ(t.kind, Token::Kind::operation);
        EXPECT_EQ(t.operation, '(');

        t = ts.next();
        EXPECT_EQ(t.kind, Token::Kind::operation);
        EXPECT_EQ(t.operation, '+');

        t = ts.next();
        EXPECT_EQ(t.kind, Token::Kind::number);
        EXPECT_DOUBLE_EQ(t.value, 0.0);

        t = ts.next();
        EXPECT_EQ(t.kind, Token::Kind::operation);
        EXPECT_EQ(t.operation, '(');

        t = ts.next();
        EXPECT_EQ(t.kind, Token::Kind::operation);
        EXPECT_EQ(t.operation, ')');

        t = ts.next();
        EXPECT_EQ(t.kind, Token::Kind::operation);
        EXPECT_EQ(t.operation, Operator::print);

        {
            t = ts.next();
            EXPECT_EQ(t.kind, Token::Kind::literal);
            auto str = t.literal.c_str();
            EXPECT_STREQ(str, "k");
        }        
        {
            t = ts.next();
            EXPECT_EQ(t.kind, Token::Kind::literal);
            auto str = t.literal.c_str();
            EXPECT_STREQ(str, "iraq");
        }


        t = ts.next();
        EXPECT_EQ(t.kind, Token::Kind::number);
        EXPECT_DOUBLE_EQ(t.value, 6.0);
    }
}



}   // namespace Unit_tests
#endif // !_LEXER_TEST_H_