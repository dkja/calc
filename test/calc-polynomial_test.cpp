#ifndef _POLYNOMIAL_TEST_H_
#define _POLYNOMIAL_TEST_H_

#include <gtest/gtest.h>
#include "parser.h"
#include "exceptions.h"
#include "tmpfstream.h"
#include "JITGenerator.h"

using namespace std;


namespace Unit_tests
{




class Parser_polynomial_test : public Parser
{
public:
    Parser_polynomial_test(Token_stream* ts, IGenerator<>& gen) : Parser(ts, gen) {}
    virtual void polynomial() override
    {
        this->Parser::polynomial();
    }
};

TEST(parser, polynomial)
{
    tmpfstream file{"/tmp/utest"};
    ASSERT_TRUE(file.good()) << "Cannot create tmpfile: " << file.filename() << '\n';

    file << "5+3;    -6*(-3);  -0*-1;   12-2*-3;    (-1+2)*-4;  8+8+8; pokemon ; 4-0+3; 0+0-0;";
    file.flush();
    file.sync();
    file.seekg(0);

    JITGenerator            jit{ cout };
    Token_stream            ts{file};
    Parser_polynomial_test  parser{&ts, jit};
    
    auto& stack = jit.get_stack();

    parser.polynomial();
    EXPECT_DOUBLE_EQ(stack.pop(), 8);

    parser.polynomial();
    EXPECT_DOUBLE_EQ(stack.pop(), 18);

    parser.polynomial();
    EXPECT_DOUBLE_EQ(stack.pop(), 0);

    parser.polynomial();
    EXPECT_DOUBLE_EQ(stack.pop(), 18);

    parser.polynomial();
    EXPECT_DOUBLE_EQ(stack.pop(), -4);

    parser.polynomial();
    EXPECT_DOUBLE_EQ(stack.pop(), 24);

    jit.add_variable("pokemon", 49.99);
    parser.polynomial();
    EXPECT_DOUBLE_EQ(stack.pop(), 49.99);


    parser.polynomial();
    EXPECT_DOUBLE_EQ(stack.pop(), 7);

    parser.polynomial();
    EXPECT_DOUBLE_EQ(stack.pop(), 0);

    
}




}   // namespace Unit_tests
#endif // !_POLYNOMIAL_TEST_H_