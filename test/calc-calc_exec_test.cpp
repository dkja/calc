#ifndef _PARSER_EXEC_TEST_H_
#define _PARSER_EXEC_TEST_H_

#include <gtest/gtest.h>
#include "parser.h"
#include "tmpfstream.h"
#include "JITGenerator.h"

using namespace std;

namespace Unit_tests
{



TEST(parser, exec_test)
{
    tmpfstream file{"/tmp/utest"};
    ASSERT_TRUE(file.good()) << "Cannot create tmpfile: " << file.filename() << '\n';

    file << "   35*4*(3-2)/9+(4/7 * 3);  \
                14--3/2/3; \
                kotlet ; \
                12345/(2-1-1);  \
                -6 * -3; \
                50-1; \
                0-1 ; \
                0-0 ;\
                25*0 ;    ";
    file.flush();
    file.sync();
    file.seekg(0);

    JITGenerator    jit{ cout };
    Token_stream    ts{file};
    Parser          parser{&ts, jit};
    
    auto& stack = jit.get_stack();

    jit.add_variable("kotlet", 567.24);
    EXPECT_NO_THROW( parser.exec(3) );
    EXPECT_EQ(stack.size(), 3);
    EXPECT_DOUBLE_EQ(stack.pop(), 567.24);
    EXPECT_DOUBLE_EQ(stack.pop(), 14.5);
    EXPECT_DOUBLE_EQ(stack.pop(), 17.269841269841269);
    
    
    EXPECT_THROW( parser.exec_all(), divide_by_zero);
    jit.get_stack().clear();

    ASSERT_NO_THROW( parser.exec_all() );
    EXPECT_EQ(stack.size(), 5);
    EXPECT_DOUBLE_EQ(stack.pop(), 0);
    EXPECT_DOUBLE_EQ(stack.pop(), 0);
    EXPECT_DOUBLE_EQ(stack.pop(), -1);
    EXPECT_DOUBLE_EQ(stack.pop(), 49);
    EXPECT_DOUBLE_EQ(stack.pop(), 18);
}

}
#endif // !_PARSER_EXEC_TEST_H_