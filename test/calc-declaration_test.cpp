#ifndef _DECLARATION_TEST_H_
#define _DECLARATION_TEST_H_

#include <gtest/gtest.h>
#include "parser.h"
#include "tmpfstream.h"
#include "JITGenerator.h"

using namespace std;
namespace Unit_tests
{



TEST(parser, declaration_test)
{

    tmpfstream file{"/tmp/utest"};
    ASSERT_TRUE(file.good()) << "Cannot create tmpfile: " << file.filename() << '\n';

    file << "   let zmienna1 = 5; \
                let variable3 = -5 * 4;  \
                let var4 = var2; \
                let var2 = 9; \
                let var4 = var2;  \
                zmienna1;\
                variable3;\
                var2;\
                var4;";
    file.flush();
    file.sync();
    file.seekg(0);

    JITGenerator    jit{ cout };
    Token_stream    ts{file};
    Parser          parser{&ts, jit};
    
    auto& stack = jit.get_stack();

    parser.exec();
    parser.exec();
    
    EXPECT_THROW(parser.exec(), var_not_found);
    
    parser.exec();
    parser.exec();
    parser.exec_all();
    
    EXPECT_DOUBLE_EQ(stack.pop(), 9);
    EXPECT_DOUBLE_EQ(stack.pop(), 9);
    EXPECT_DOUBLE_EQ(stack.pop(), -20);
    EXPECT_DOUBLE_EQ(stack.pop(), 5);

}


}   // namespace Unit_tests
#endif // !_DECLARATION_TEST_H_