#ifndef _URANDOM_TEST_H_
#define _URANDOM_TEST_H_

#include <gtest/gtest.h>
#include <fstream>
#include "parser.h"
#include "JITGenerator.h"


using namespace std;

namespace Unit_tests
{


/**
 *	Lexer unit test
 */

TEST(urandom_test, exec_all)
{

    ifstream test_file{"/dev/urandom"};
    ASSERT_TRUE(test_file.good()) << "Cannot open /dev/urandom";


    JITGenerator jit{ cout };
    Token_stream ts{test_file};
    Parser parser{&ts, jit};

    ASSERT_ANY_THROW(parser.exec_all());


}



}   // namespace Unit_tests
#endif // !_URANDOM_TEST_H_