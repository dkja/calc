#ifndef _VARIABLE_TEST_H_
#define _VARIABLE_TEST_H_

#include <gtest/gtest.h>
#include "parser.h"
#include "tmpfstream.h"
#include "JITGenerator.h"

using namespace std;
namespace Unit_tests
{


TEST(parser, variables)
{

    tmpfstream file{"/tmp/utest"};
    ASSERT_TRUE(file.good()) << "Cannot create tmpfile: " << file.filename() << '\n';

    file << "   let a = 5; \
                let b = 9; \
                a+b; \
                a-b; \
                a*b; \
                a/b;    ";
    file.flush();
    file.sync();
    file.seekg(0);

    JITGenerator    jit{ cout };
    Token_stream    ts{file};
    Parser          parser{&ts, jit};
    
    auto& stack = jit.get_stack();

    parser.exec();
    EXPECT_EQ(stack.size(), 0);

    parser.exec();
    EXPECT_EQ(stack.size(), 0);
    
    parser.exec_all();
    EXPECT_EQ( stack.size(), 4);
    EXPECT_DOUBLE_EQ(stack.pop(), (5.0/9.0));
    EXPECT_DOUBLE_EQ(stack.pop(), 45);
    EXPECT_DOUBLE_EQ(stack.pop(), -4);
    EXPECT_DOUBLE_EQ(stack.pop(), 14);
    

}


}   // namespace Unit_tests
#endif // !_VARIABLE_TEST_H_