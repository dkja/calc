#ifndef _MONOMIAL_TEST_H_
#define _MONOMIAL_TEST_H_

#include <gtest/gtest.h>
#include "parser.h"
#include "exceptions.h"
#include "tmpfstream.h"
#include "JITGenerator.h"

using namespace std;


namespace Unit_tests
{




class Parser_monomial_test : public Parser
{
public:
    Parser_monomial_test(Token_stream* ts, IGenerator<>& gen) : Parser(ts, gen) {}
    virtual void monomial() override
    {
        return this->Parser::monomial();
    }
};

TEST(parser, monomial_test)
{
    tmpfstream file{"/tmp/utest"};
    ASSERT_TRUE(file.good()) << "Cannot create tmpfile: " << file.filename() << '\n';

    file << "5*3;    -6*3;  0*1;   12*-3; -1*-4;  8*8; 4/0; 0*0;";
    file.flush();
    file.sync();
    file.seekg(0);

    JITGenerator            jit{ cout };
    Token_stream            ts{file};
    Parser_monomial_test    parser{&ts, jit};
    
    auto& stack = jit.get_stack();

    parser.monomial();
    EXPECT_DOUBLE_EQ(stack.pop(), 15);

    parser.monomial();
    EXPECT_DOUBLE_EQ(stack.pop(), -18);

    parser.monomial();
    EXPECT_DOUBLE_EQ(stack.pop(), 0);

    parser.monomial();
    EXPECT_DOUBLE_EQ(stack.pop(), -36);

    parser.monomial();
    EXPECT_DOUBLE_EQ(stack.pop(), 4);

    parser.monomial();
    EXPECT_DOUBLE_EQ(stack.pop(), 64);

    EXPECT_THROW( parser.monomial(), divide_by_zero);

    parser.monomial();
    EXPECT_DOUBLE_EQ(stack.pop(), 0);
    
}



}   // namespace Unit_tests
#endif // !_MONOMIAL_TEST_H_