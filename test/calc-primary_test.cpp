#ifndef _PRIMARY_TEST_H_
#define _PRIMARY_TEST_H_

#include <gtest/gtest.h>
#include "parser.h"
#include "tmpfstream.h"
#include "JITGenerator.h"

using namespace std;


namespace Unit_tests
{



class Parser_primary_test : public Parser
{
public:
    Parser_primary_test(Token_stream* ts, IGenerator<>& gen) : Parser(ts, gen) {}
    virtual void primary() override
    {
        this->Parser::primary();
    }
};

TEST(parser, primary)
{
    tmpfstream file{"/tmp/utest"};
    ASSERT_TRUE(file.good()) << "Cannot create tmpfile: " << file.filename() << '\n';

    file << "5; ; 6; 8; 10; -4; 0; -0; 7; kon; -1; xeon ; 2;";
    file.flush();
    file.sync();
    file.seekg(0);

    JITGenerator        jit{ cout };
    Token_stream        ts{file};
    Parser_primary_test parser{&ts, jit};
    
    auto& stack = jit.get_stack();

    parser.primary();
    EXPECT_DOUBLE_EQ(stack.pop(), 5);
    
    EXPECT_THROW( parser.primary(), parser_error);

    parser.primary();
    EXPECT_DOUBLE_EQ(stack.pop(), 6);
    
    parser.primary();
    EXPECT_DOUBLE_EQ(stack.pop(), 8);
    
    parser.primary();
    EXPECT_DOUBLE_EQ(stack.pop(), 10);
    
    parser.primary();
    EXPECT_DOUBLE_EQ(stack.pop(), -4);
    
    parser.primary();
    EXPECT_DOUBLE_EQ(stack.pop(), 0);
    
    parser.primary();
    EXPECT_DOUBLE_EQ(stack.pop(), -0);
    
    parser.primary();
    EXPECT_DOUBLE_EQ(stack.pop(), 7);
    
    EXPECT_THROW(parser.primary(), var_not_found);

    parser.primary();
    EXPECT_DOUBLE_EQ(stack.pop(), -1); 
    
    
    jit.add_variable("xeon", 99);
    parser.primary();
    EXPECT_DOUBLE_EQ(stack.pop(), 99); 
    

    parser.primary();
    EXPECT_DOUBLE_EQ(stack.pop(), 2); 
}



}   // namespace Unit_tests
#endif // !_PRIMARY_TEST_H_

